#!/bin/bash

# Filename and paths where to look for the original ETQW ISO
ISONAME="${ISONAME:-ETQWPCDVD.iso}"
ISOPATH="${ISOPATH:-"$PWD $PWD/../"}"
ISOMOUNTPATH="$(mktemp -d "/tmp/AppRun-${APPPKG}-ETQWPCDVD.XXXXXXXXXX")"
export PATH="$APPDIR/usr/bin/:$PATH"

found=
for path in $ISOPATH; do
	iso="$path/$ISONAME"
	if [ -f $iso ]; then
		echo "Mounting $iso in $ISOMOUNTPATH"
		fuseiso "$iso" "$ISOMOUNTPATH" || exit 1
		BINARY_ARGS+="+set fs_cdpath $ISOMOUNTPATH/Setup/Data"
		function cleanup_mount() {
			echo "Unmounting $ISOMOUNTPATH"
			fusermount -u "$ISOMOUNTPATH"
			rmdir "$ISOMOUNTPATH"
		}
		trap cleanup_mount EXIT
		found=1
		break
	fi
done

if [ ! $found ]; then
	echo "File $ISONAME not found in these directories: $ISOPATH"
	exit 1
fi

if [ ! -d "$ISOMOUNTPATH/Setup/Data" ]; then
	echo "File $ISONAME seems to be an invalid ETQW ISO, it doesn't seem to contain directory Setup/Data"
	exit 1
fi

